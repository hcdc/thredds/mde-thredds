# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for thredds-control-center."""

from typing import Callable

import pytest  # noqa: F401
import requests

from tds_control import app_settings, models


@pytest.fixture
def tdsserver(db) -> models.TDSServer:
    """Create a thredds server"""

    return models.TDSServer.objects.get_or_create(name="thredds")[0]


@pytest.fixture
def tds_rf() -> Callable[[str], requests.Response]:
    """Get a request factory to query the thredds server."""

    def rf(location: str):
        """Query the thredds server."""
        url = app_settings.TDS_TOMCAT_PUBLIC_URL + location
        return requests.get(url)

    return rf


@pytest.fixture(scope="session")
def django_db_setup():
    pass
